package banque;

import java.util.ArrayList;
import java.util.Observable;

public class BanqueModel extends Observable {
	
	private String name;
	private ArrayList<Client> clients;
	private ArrayList<Compte> comptes;
	private int id;
	
	
	
	public BanqueModel() {
		this.clients = new ArrayList<Client>();
		this.comptes = new ArrayList<Compte>();
		this.id = 0;
	}
	
	
	
	public void createClient(Client client, Compte compte) {
		compte.setDetenteur(client);
		clients.add(client);
		comptes.add(compte);
		this.id++;
		
		setChanged();
		notifyObservers();
	}
	
	
	public int getId() {
		return this.id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Client> getClients() {
		return clients;
	}
	public void setClients(ArrayList<Client> clients) {
		this.clients = clients;
	}
	public ArrayList<Compte> getComptes() {
		return comptes;
	}
	public void setComptes(ArrayList<Compte> comptes) {
		this.comptes = comptes;
	}
	
	
	
	
	

}
