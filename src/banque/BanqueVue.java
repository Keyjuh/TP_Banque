package banque;

import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

public class BanqueVue extends JFrame implements Observer {
	
	private BanqueModel model;
	private BanqueController controller;
	
	
	public BanqueVue (BanqueModel model, BanqueController controller) {
		super();
		
		this.model = model;
		this.controller = controller;
		
		setTitle(model.getName());
		setBounds(100, 100, 600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		this.model.addObserver(this);
		this.controller.createClient(this.model, new Client(this.model.getId(), "CILES", "Thomas"), new Compte(this.model.getId(), 0.0f));
	}
	

	
	
	
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println("Nouveau client créé");
		System.out.println("Prochain ID -> " + this.model.getId());
		
		repaint();
	}
	
	
	
	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
	}

}
