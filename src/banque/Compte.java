package banque;

public class Compte {
	
	
	private int numero;
	private float solde;
	private Client detenteur;
	
	
	
	public Compte(int numero, float solde) {
		this.numero = numero;
		this.solde = solde;
	}
	
	
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public float getSolde() {
		return solde;
	}
	public void setSolde(float solde) {
		this.solde = solde;
	}
	public Client getDetenteur() {
		return detenteur;
	}
	public void setDetenteur(Client detenteur) {
		this.detenteur = detenteur;
	}
	
	
	
	

}
