package banque;

import javax.swing.JFrame;

public class Launcher {
	
	public static void main(String[] args) {
		BanqueModel model = new BanqueModel();
		BanqueController controller = new BanqueController();
		
		JFrame banque = new BanqueVue(model, controller);
		banque.setVisible(true);
	}

}
